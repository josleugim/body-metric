/**
 * Created by josleugim on 5/4/15.
 */
var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');

//open the connection
var db = mongoose.connect('mongodb://localhost/body-metric');

//import models
var Measure = require('./models/measureModel');
var Member = require('./models/v1/memberModel');

//environment config
var app = express();
var port = process.env.PORT || 3000;

//configure the body parser
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(express.static('views'));
app.use(express.static('static'));

//injecting the models to the router
memberRouter = require('./routes/v1/memberRoutes')(Member);
app.use('/api/v1/members', memberRouter);

measureRouter = require('./routes/measureRoutes')(Measure);
app.use('/api/v1/measures', measureRouter);

//default route
app.get('/', function (req, res) {
    res.sendFile('index.html');
});

//open port
app.listen(port, function () {
    console.log('Gulp is running my app on PORT: ' + port);
})