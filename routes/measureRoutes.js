/**
 * Created by root on 5/5/15.
 */
var express = require('express');

var routes = function (Measure) {
    var measureRouter = express.Router();
    measureRouter.route('/')
        .post(function (req, res) {
            var measure = new Measure(req.body);
            console.log(req.body);
            measure.save(function (err) {
                if(err) res.status(500).send(err);
            });
            res.status(201).send(measure);
        })
        .get(function (req, res) {
            Measure.find(function (err, measures) {
                if(err)
                    res.status(500).send(err);
                else
                    res.json(measures);
            });
        });

    return measureRouter;
};

module.exports = routes;