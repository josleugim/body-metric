/**
 * Created by root on 5/5/15.
 */
var express = require('express');

var routes = function (Member) {
    var memberRouter = express.Router();

    memberRouter.route('/signin')
        .post(function (req, res) {
            var query = Member.where({email: req.body.email});

            query.findOne(function (err, member) {
                if(err) res.status(500).send(err);

                if(member)
                    res.status(409).send({message: 'Cuenta existente'});
                else {
                    var newMember = new Member(req.body);
                    newMember.save(function (err) {
                        if(err) res.status(500).send(newMember);
                    });

                    res.status(201).send(newMember);
                }
            });
        })

    memberRouter.route('/:_id')
        .get(function (req, res) {
            Member.findById(req.params._id, function (err, member) {
                if(err)
                    res.status(500).send(err);
                else
                    res.json(member);
            });
        });
    return memberRouter;
};

module.exports = routes;