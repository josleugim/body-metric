/**
 * Created by josleugim on 5/5/15.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var memberModel = new Schema({
    name: {type: String},
    email: {type: String},
    birthday: {type: Date},
    height: {type: Number},
    isActive: {type: Boolean},
    createdAt: {type: Date, default: Date.now()},
    updatedAt: {type: Date}
});

module.exports = mongoose.model('Member', memberModel);