/**
 * Created by josleugim on 5/4/15.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var measureModel = new Schema({
    _member: {type: Schema.ObjectId, ref: 'Member'},
    neck: {type: Number},
    abs: {type: Number},
    glutes: {type: Number},
    thighs: {
        left: {type: Number},
        right: {type: Number}
    },
    calves: {
        left: {type: Number},
        right: {type: Number}
    },
    biceps: {
        left: {type: Number},
        right: {type: Number}
    },
    forearms: {
        left: {type: Number},
        right: {type: Number}
    },
    triceps: {
        left: {type: Number},
        right: {type: Number}
    },
    createdAt: {type: Date, default: Date.now()},
    updatedAt: {type: Date}
});

module.exports = mongoose.model('Measure', measureModel);